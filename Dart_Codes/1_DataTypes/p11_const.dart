
// const variables in dart

main(){

  const int x = 100;
  print(x);

  //x = 20;  //Error: Can't assign to the const variable 'x'.

  const y = 10; // automatically recognized datatypes

  print(y);
  print(y.runtimeType);
  
}