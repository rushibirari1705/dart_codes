
// All Datatypes in dart

/* 1. int
   2. double
   3. num
   4. var
   5.String
   
 */

main(){
  print("All Datatypes in dart : ");
  // int
  int x = 100;
  print( "int  :  $x " );
  

  double y = 100.98;
  print("double : $y ");

  num z = 100.98;
  print( "num : $z" );

  var a = 1234.21;
  print("var double : $a ");

  var b = "Rushi";
  print("var String : $b");

  String name = "Rushi Birari";
  print("String : $name");

  bool flag = false;
  print(flag);

}