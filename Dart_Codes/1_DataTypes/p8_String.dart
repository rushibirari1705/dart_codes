
main(){
  // String is a class 

  String name = "Rushi"; // use double quotes 
  print(name);

  String lastname = 'Birari'; // use single quotes
  print(lastname);

  print(name + " " + lastname);
}