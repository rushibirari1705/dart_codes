// var is a datatype which stores any type of data 

main(){

  // var 

  var x = 10;
  print(x);

  x = 20;
  print(x);

  var y = "Shashi";
  print(y.runtimeType);

  //x = "Rushi"; Error: A value of type 'String' can't be assigned to a variable of type 'int'.
  // Once you gave datatype  to var then it always accepts only that type of data

}