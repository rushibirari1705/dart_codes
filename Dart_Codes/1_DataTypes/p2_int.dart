
// int is also class in dart
void main() {

  print("int datatype in dart..");

  // int x = 10.0; Error: A value of type 'double' can't be assigned to a variable of type 'int'.

  // int x = true; Error: A value of type 'bool' can't be assigned to a variable of type 'int'.

  //int x = "rushi"; Error: A value of type 'String' can't be assigned to a variable of type 'int'.

  int x = 11;
  print("value of x :  $x");
  
}