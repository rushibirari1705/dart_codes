
// dynamic datatype is used to store the data that changed at runtime in program

main(){

  // dynamic datatype;
  dynamic x = 10;
  dynamic y = 20;
  print(x);
  print(y);

  print(x.runtimeType);
  print(y.runtimeType);

  x = 30.4;
  y = 23.44;

  print(x);
  print(y);

  print(x.runtimeType);
  print(y.runtimeType);

  x = "Rushi";
  y = 'A';
  print(x);
  print(y);

  print(x.runtimeType);
  print(y.runtimeType);

}