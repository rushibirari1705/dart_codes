
main(){

  // num is datatype which store two types of data int & double

  num x = 10;

  print(x);

  x = 34.455; // assigning double value to x 

  print(x);

   //x = "Rushi"; Error: A value of type 'String' can't be assigned to a variable of type 'num'.

}