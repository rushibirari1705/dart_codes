
// if-else control statement in dart

main(){

   num x = 10;
   int y = 20;

  if(x == y)
    print("Both are equal");
  else if(x > y)
    print("x is greater than y");
  else{
    print("y is greater than x");
  }
 }