
// while loop concept in dart it is also known as entry-controlled loop

main(){
  // while loop

  int i = 1;
  while(i <= 10){

    print(i);
    i++;
  }
}