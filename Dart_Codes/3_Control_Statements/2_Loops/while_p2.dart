
// Using while loop find odd numbers between 1 to 50

main(){
  int i = 1;
  while(i <= 50){
    if(i % 2 == 1)
    print(i);
    i += 3;
  }
}