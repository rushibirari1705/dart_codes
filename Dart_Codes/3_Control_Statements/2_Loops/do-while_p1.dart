
// do-while loop concept in dart(also known as Exit-Controlled loop)

main(){
  // do-while loop 
  int i = 1 ;
  do{
    print("Hello world");
    i++;

  }while(i <= 10);
}