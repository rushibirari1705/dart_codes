
Inheritance

================================================================
//Program 1 :

class Test {
  int x = 10;
  Test(this.x);
}

class Test2 extends Test {
  Test2(super.x);
}

void main() {

  Test2 obj = Test2(10);
  Test obj2 = Test(30);

  obj.x = 19;

  print(obj.x);
  print(obj2.x);
}

Output : 
      19
      30

Explaination : 

    In above example, the test1 class has x = 10  & class test2 have constructor which passes the x value to
    parent constructor by using super.
    when obj is created we pass the 10 value to x  by test2 constructor and store x = 10
    and obj2 pass 30 to parent constructor using super.x
    but obj.x = 19 change will we done only for this object not for all other objects

========================================================================
// Program2 :

class Test {
  int x = 30;
}

class Test2 extends Test {
  int x;
  Test2(this.x);
  void gun() {
    this.x = 8;
  }

  void fun() {
    print(this.x);
    print(super.x);
  }
}

void main() {
  Test2 obj = Test2(10);
  obj.gun();
  obj.fun();
}

Output : 
      8
      30

Explaination : 
    In this example, Class Test have x = 30, Class Test2 extends Test
    but when object is created, we pass 10 to test2 and store in x of test2 by constructor
    after that we call gun() where we update value of x ,(x  = 8 )
    then call fun() it prints the latest value of x is 8: and x of parent class is 30
    i.e o/p is 8 & 30
========================================================================
// prrogram 3 :

class Test {
  int x = 30;
  int y = 30;
}

class Test2 extends Test {
  int x;

  Test2(this.x);

  void gun() {
    this.x = 8;
    this.y = 19;
  }

  void fun() {
    print(super.x);
    print(super.y);
  }

}

void main() {
    Test2 obj = Test2(10);

    obj.gun();
    obj.fun();
  }

  Output : 
        30
        19

  Explaination : 
    In above example , the class Test has two variables x = 30 & y = 30
    and class test2 extends Test class and have its own variable x;

    when we create a instance of the class we pass 10 to constructor of test2 where x becomes x = 10
    then call gun() which updates the variable values x = 8 & y = 19
    after we call fun() then it prints x & y values using super i.e 30 19 is print 19 is updated value of
    x in that class and y is inherited from parent class

========================================================================

// Program 4 :

class Test {
  int x;
  int? y;

  Test({required this.x, this.y});

}

class Test2 extends Test {

  Test2(int x, int y) : super(x: 88) {
    print(x);
  }
  void fun() {
    print(this.x);
    print(super.x);
  }

}

void main() {

  Test2 obj = Test2(19, 20);
  obj.fun();

}

output : 
    19
    88
    88

Explaination :
    In above example, Test2(19, 20) pass value to Test2 constructor which have x & y and also
    super is used to pass the value to x of parent class which is required paramater to constructor.
    we print x in constructor thats why o/p is 19 on first line after that the we call fun() which prints
    this.x and super.x it prints 88 & 88 beacuse x is inherited from parent class x = 88 and parent class has x = 88

    so it prints 19 88 88

========================================================================

// program 5 : 

class Test{
    int x=20;
    String str="Core2web";
    
    void parentMethod(){
      print(x);
      print(str);

    }
}
class Test2 extends Test{ 
    int x=10;
    String str="Incubator";

    void childMethod(){
      print(x);
      print(str);
    }
}
void main(){
    Test2 obj=new Test2();
    obj.parentMethod();
    obj.childMethod();
}

  Output : 
    10
    Incubator
    10       
    Incubator

  Explaination : 
    In above example, the both test and test2 has two variables and one method we created a object
    of test2 class then call method parentMethod() which is inherited from parent class so it access the variables
    of its own class so they print 10,"Incubator" and also childMethod() also prints 10,"Incubator".

========================================================================
//Program 6 :

class Test {
  int? x;

  Test(this.x) {
    print("In test");
  }

}

class Test2 extends Test {
  int? y;

  Test2(this.y, int x) : super(x);

}

class Test3 extends Test2 {
  int? z;
  
  Test3(this.z, int y, int x) : super(y, x) {
    print("In test3");
  }

}

void main() {
  Test3 obj = Test3(10, 20, 30);
}

  Output :
    In test
    In test3

  Explaination : 
      In above example, object of Test3 is created and pass 3 values 10,20,30 then test3 constructor 
      takes only 10 value and store in its own z variable. and pass two y & x to super class and then it also
      pass x to the super class and test constructor print test and test3 constructor print test3
      so, o/p is test & test3

========================================================================
// Program 7 :

class Demo1 {
  int x;
  Demo1(this.x);

}

class Demo2 extends Demo1 {
  Demo2(super.x);
  
  void fun() {
    print(x);
  }

}

void main() {
// Demo obj1 = Demo2();
  Demo1 obj2 = Demo2(10);
  obj2.fun();
}

  Output :
      Error: The method 'fun' isn't defined for the class 'Demo1'.
  - 'Demo1' is from 'p7.dart'.
    Try correcting the name to the name of an existing method, or defining a method named 'fun'.
    obj2.fun(); 

  Explaination : 

  In above example , Error is because of we create a object with parent reference  and childs object 
  and compiler checks the method at the compile time. then compiler does not see the fun() method in Parent class
  so, it gives an error.

========================================================================
    
// Program 8  :
  class Parent {
    Parent() {
      print("In Parent Constructor");
    }
  }
  class Child extends Parent {

    Child() {
      super();
      print("In Child Constructor");
    }
  }
  void main() {
    Child obj = new Child();
  }

    Output :
      Error: Superclass has no method named 'call'.
      super();
      ^^^^
    
    Explaination  :

    When we create a object then it implicitly call constructor then we cannot call direct super() in constructor
    so , we need to add call() method to the Parent class them it will work.
  
========================================================================
// Program 9 :

class Test {
  int? x;
  static int y = 20;
  
  Test.initX(this.x);
  
  static void changeY() {
    y = 30;
  }

}

class Test2 extends Test {

  Test2(int x) : super.initX(x);

}

void main() {
  Test2 obj = Test2(40);
  Test2.changeY();
}

Output : 
    Error: Member not found: 'Test2.changeY'.
    Test2.changeY();
        ^^^^^^^

Explaination  :
    In this example, we create object and pass value to constructor then it passes to parent constructor
    by using super the member 'Test2.changeY' is not found because static member are not inherited from 
    parent class.

========================================================================
// Program 10  :

abstract class Demo1 {
  factory Demo1() {
    return Demo2();
  }
}

class Demo2 implements Demo1 {
  Demo2() {
    print("Demo2");
  }
}

void main() {
  Demo1 obj = new Demo1();
}

  Output : 
      Demo2

  Explaination : 
   In this example, one is abstract class which have factory constructor which 
   returns a object of child class then call then constructor of child class and print Demo2

