// Program2 :

class Test {
  int x = 30;
}

class Test2 extends Test {
  int x;
  Test2(this.x);
  void gun() {
    this.x = 8;
  }

  void fun() {
    print(this.x);
    print(super.x);
  }
}

void main() {
  Test2 obj = Test2(10);
  obj.gun();
  obj.fun();
}

/*
Output : 
      8
      30

Explaination : 
    In this example, Class Test have x = 30, Class Test2 extends Test
    but when object is created, we pass 10 to test2 and store in x of test2 by constructor
    after that we call gun() where we update value of x ,(x  = 8 )
    then call fun() it prints the latest value of x is 8: and x of parent class is 30
    i.e o/p is 8 & 30
*/
