
// Program 8  :
  class Parent {
    Parent() {
      print("In Parent Constructor");
    }
    
  }
  class Child extends Parent {

    Child() {
      super();
      print("In Child Constructor");
    }
  }
  void main() {
    Child obj = new Child();
  }

  /*
    Output :
      Error: Superclass has no method named 'call'.
      super();
      ^^^^
    
    Explaination  :

    When we create a object then it implicitly call constructor then we cannot call direct super() in constructor
    so , we need to add call() method to the Parent class them it will work.
  */