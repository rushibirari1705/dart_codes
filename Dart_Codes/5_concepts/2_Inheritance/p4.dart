// Program 4 :

class Test {
  int x;
  int? y;

  Test({required this.x, this.y});

}

class Test2 extends Test {

  Test2(int x, int y) : super(x: 88) {
    print(x);
  }
  void fun() {
    print(this.x);
    print(super.x);
  }

}

void main() {

  Test2 obj = Test2(19, 20);
  obj.fun();

}

/*
output : 
    19
    88
    88

Explaination :
    In above example, Test2(19, 20) pass value to Test2 constructor which have x & y and also
    super is used to pass the value to x of parent class which is required paramater to constructor.
    we print x in constructor thats why o/p is 19 on first line after that the we call fun() which prints
    this.x and super.x it prints 88 & 88 beacuse x is inherited from parent class x = 88 and parent class has x = 88

    so it prints 19 88 88
*/
