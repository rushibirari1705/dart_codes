// Program 10  :

abstract class Demo1 {
  factory Demo1() {
    return Demo2();
  }
}

class Demo2 implements Demo1 {
  Demo2() {
    print("Demo2");
  }
}

void main() {
  Demo1 obj = new Demo1();
}

/*
  Output : 
      Demo2

  Explaination : 
   In this example, one is abstract class which have factory constructor which 
   returns a object of child class then call then constructor of child class and print Demo2
*/
