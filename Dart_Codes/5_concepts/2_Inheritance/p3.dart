// prrogram 3 :

class Test {
  int x = 30;
  int y = 30;
}

class Test2 extends Test {
  int x;

  Test2(this.x);

  void gun() {
    this.x = 8;
    this.y = 19;
  }

  void fun() {
    print(super.x);
    print(super.y);
  }

}

void main() {
    Test2 obj = Test2(10);

    obj.gun();
    obj.fun();
  }

/*
  Output : 
        30
        19

  Explaination : 
    In above example , the class Test has two variables x = 30 & y = 30
    and class test2 extends Test class and have its own variable x;

    when we create a instance of the class we pass 10 to constructor of test2 where x becomes x = 10
    then call gun() which updates the variable values x = 8 & y = 19
    after we call fun() then it prints x & y values using super i.e 30 19 is print 19 is updated value of
    x in that class and y is inherited from parent class

*/