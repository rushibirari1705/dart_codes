// Program 7 :

class Demo1 {
  int x;
  Demo1(this.x);

}

class Demo2 extends Demo1 {
  Demo2(super.x);
  
  void fun() {
    print(x);
  }

}

void main() {
// Demo obj1 = Demo2();
  Demo1 obj2 = Demo2(10);
  obj2.fun();
}

/*
  Output :
      Error: The method 'fun' isn't defined for the class 'Demo1'.
  - 'Demo1' is from 'p7.dart'.
    Try correcting the name to the name of an existing method, or defining a method named 'fun'.
    obj2.fun(); 

  Explaination : 

  In above example , Error is because of we create a object with parent reference  and childs object 
  and compiler checks the method at the compile time. then compiler does not see the fun() method in Parent class
  so, it gives an error.

*/
