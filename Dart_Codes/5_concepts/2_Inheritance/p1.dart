//Program 1 :

class Test {
  int x = 10;
  Test(this.x);
}

class Test2 extends Test {
  Test2(super.x);
}

void main() {

  Test2 obj = Test2(10);
  Test obj2 = Test(30);

  obj.x = 19;

  print(obj.x);
  print(obj2.x);
}

/*
Output : 
      19
      30

Explaination : 

    In above example, the test1 class has x = 10  & class test2 have constructor which passes the x value to
    parent constructor by using super.
    when obj is created we pass the 10 value to x  by test2 constructor and store x = 10
    and obj2 pass 30 to parent constructor using super.x
    but obj.x = 19 change will we done only for this object not for all other objects
*/