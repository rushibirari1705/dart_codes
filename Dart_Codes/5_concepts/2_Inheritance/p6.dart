//Program 6 :

class Test {
  int? x;

  Test(this.x) {
    print("In test");
  }

}

class Test2 extends Test {
  int? y;

  Test2(this.y, int x) : super(x);

}

class Test3 extends Test2 {
  int? z;
  
  Test3(this.z, int y, int x) : super(y, x) {
    print("In test3");
  }

}

void main() {
  Test3 obj = Test3(10, 20, 30);
}

/*
  Output :
    In test
    In test3

  Explaination : 
      In above example, object of Test3 is created and pass 3 values 10,20,30 then test3 constructor 
      takes only 10 value and store in its own z variable. and pass two y & x to super class and then it also
      pass x to the super class and test constructor print test and test3 constructor print test3
      so, o/p is test & test3
*/
