
// program 5 : 

class Test{
    int x=20;
    String str="Core2web";
    
    void parentMethod(){
      print(x);
      print(str);

    }
}
class Test2 extends Test{ 
    int x=10;
    String str="Incubator";

    void childMethod(){
      print(x);
      print(str);
    }
}
void main(){
    Test2 obj=new Test2();
    obj.parentMethod();
    obj.childMethod();
}
/*
  Output : 
    10
    Incubator
    10       
    Incubator

  Explaination : 
    In above example, the both test and test2 has two variables and one method we created a object
    of test2 class then call method parentMethod() which is inherited from parent class so it access the variables
    of its own class so they print 10,"Incubator" and also childMethod() also prints 10,"Incubator".
*/