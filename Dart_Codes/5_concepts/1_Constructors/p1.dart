
// Constructors 
// program1 :

class Test{
  final int x;
  final int y;

  const Test(this.x, this.y){
    print("In constant Construtor");
  }

}

void main(){
  Test obj = new Test(10,20);
  print(obj.x);

}

/* 
  Error :  Error: A const constructor can't have a body.
            Try removing either the 'const' keyword or the body.      
              const Test(this.x, this.y){
  ^^^^^
  In this program :
    constant constructor does not have a body

    


*/