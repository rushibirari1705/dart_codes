
// program 8 : 

class Player{
  int? jerNo;
  String? pName ;

  const Player(this.jerNo, this.pName);

}

void main(){
  Player obj = new Player(45, "Rohit");
}

/*
output : 

  Error: Constructor is marked 'const' so all fields must be final.
  const Player(this.jerNo, this.pName);
        ^
  p8.dart:5:8: Context: Field isn't final, but constructor is 'const'.
    int? jerNo;
        ^
  p8.dart:6:11: Context: Field isn't final, but constructor is 'const'.
    String? pName ;

Explaination : 
    In above proble we write a const constructor which requires all fields to be final, but above both are 
    non-final variables i.e it gives error

    If we want to remove error we need to decalare varibles as final pr remove const from construtor.
*/