
// Constructors 

// program1 :

class Test{
  final int x;
  final int y;

  const Test(this.x, this.y){
    print("In constant Construtor");
  }

}

void main(){
  Test obj = new Test(10,20);
  print(obj.x);

}

output: 
  Error :  Error: A const constructor can't have a body.
            Try removing either the 'const' keyword or the body.      
              const Test(this.x, this.y){
  ^^^^^
Explaination :
      In this program constant constructor does not have a body

================================================================

// problem 2:

class Employee{
  int? empId;
  String? empName;

  Employee(){}

  Employee(int? empId, String? empName){}

}

void main(){
  Employee obj = new Employee();
}


output : 
    Error : 'Employee' is already declared in this scope.
      Employee(int? empId, String? empName){}
      ^^^^^^^^
    p2.dart:8:3: Context: Previous declaration of 'Employee'.
      Employee(){}
      ^^^^^^^^
    p2.dart:15:22: Error: Can't use 'Employee' because it is declared more than once.
      Employee obj = new Employee();

Explaination : 
    In this program we cannot use same named constructor in dart & also Employee constructor is declared more than once 
    that's why compiler gives error.

    solution : for this problem is used Named Constructor 

================================================================
// program 3: 

class Demo{
  final int? x;
  final String? str;

  const Demo(this.x, this.str);

}
void main(){

  Demo obj1 = const Demo(10,"Core2web");

  print(obj1.hashCode);

  Demo obj2 = const Demo(10,"Biencaps");

  print(obj2.hashCode);
}

output : 889665570
         651191664
    
Explaination:
    This program gives hashCode for both objects and both are different from each other
    we use const constructor and see the hashCode of multiple objects  

================================================================


// Program 4 :

class Company{
  int empCount;
  String compName;

  Company(this.empCount, [this.compName = "Biencaps"]);

  void compInfo(){
    print(empCount);
    print(compName);
  }
}

void main(){
  Company obj1 = new Company(100);

  Company obj2 = new Company(200,"Pubmatic");

  obj1.compInfo();
  obj2.compInfo();
}

Output :  100
          Biencaps
          200     
          Pubmatic

Explaination : 
        In this problem Class Company has Constructor with two parameters among them one is default value
        if the user doesn't give a value to constructor them the default value is considered and store value 
        particular variable
        compName is given with default value so in Obj1 = we pass only empCount so they second value is considered
        default value:

================================================================

// Problem 5 :

class Company{
  int? x;
  String? str;

  Company(this.x,{this.str = "Core2web"});

  void compInfo(){
    print(x);
    print(str);
  }
}

void main(){
  Company obj1 = new Company(100);

  Company obj2 = new Company(200,"Incubator");

  obj1.compInfo();
  obj2.compInfo();
}


output : 
  Error: Too many positional arguments: 1 allowed, but 2 found.
  Try removing the extra positional arguments.
  Company obj2 = new Company(200,"Incubator");

  p5.dart:8:3: Context: Found this candidate, but the arguments don't match.  
  Company(this.x,{this.str = "Core2web"});

Explaination : 
    In this program we create a constructor with 

================================================================

// problem 6 :
class Company{
  int? empCount;
  String? compName;

  Company({this.empCount,this.compName = "Core2web"});

  void compInfo(){
    print(empCount);
    print(compName);
  }
}

void main(){
  Company obj1 = new Company(empCount : 100, compName: "Veritas");

  Company obj2 = new Company(compName: "Pubmatic",empCount: 200);

  obj1.compInfo();
  obj2.compInfo();
}


  Output : 
    100
    Veritas 
    200     
    Pubmatic

  Explaination : 
      In this program there is concept of Named Argument wher we can change the sequence of arguments
      while passing to constructor so it directly match the varible name and store value to intance variable

================================================================

//program 7 :

class Point {
  int x;
  int y;
}

void main() {
  Point obj = new Point();
}


Output : 
  Error: Field 'x' should be initialized because its type 'int' doesn't allow null.
  Error: Field 'y' should be initialized because its type 'int' doesn't allow null.

Explaination : 

Both variables are not initialized and int does not allow null values.
so need to give value to both variables

================================================================

// program 8 : 

class Player{
  int? jerNo;
  String? pName ;

  const Player(this.jerNo, this.pName);

}

void main(){
  Player obj = new Player(45, "Rohit");
}

output : 

  Error: Constructor is marked 'const' so all fields must be final.
  const Player(this.jerNo, this.pName);
        ^
  p8.dart:5:8: Context: Field isn't final, but constructor is 'const'.
    int? jerNo;
        ^
  p8.dart:6:11: Context: Field isn't final, but constructor is 'const'.
    String? pName ;

Explaination : 
    In above proble we write a const constructor which requires all fields to be final, but above both are 
    non-final variables i.e it gives error

    If we want to remove error we need to decalare varibles as final pr remove const from construtor.

================================================================


//Problem : 9


int a = 10;
class Test{
  int x = 20;
  int y = 20;
  static num z = 30;

  Test(this.x, this.y,this.a);

  void fun(){
    print(a);
    print(x);
    print(y);
  }
}

void main(){
  Test obj = new Test(10,30,40);
  obj.fun();
}

Output :
      Error: 'a' isn't an instance field of this class.
      Test(this.x, this.y,this.a);

Explaination : 
      In above code , a declared as global variable and x,y,z are intance variables of Test class 
      so we access the a in constructor which is a global and it is instance foeld of class so 
      we use only variables that are in class

============================================================

//Program 10 : 

class Demo{

    Demo(){
      print("In demo");
    }

    factory Demo(){
      print("In demo factory");
      return Demo();
    }
}
void main(){

  Demo obj = new Demo();

}


Output: 
    Error: 'Demo' is already declared in this scope.
    factory Demo(){
            ^^^^
      Context: Previous declaration of 'Demo'.
    Demo(){
    ^^^^
    Error: Can't use 'Demo' because it is declared more than once.
      return Demo();
             ^
    Error: Can't use 'Demo' because it is declared more than once.
  Demo obj = new Demo();

Explaination:

  In this example , The Demo costructor in declared more than once so it gives error

============================================================

//Program 11: 

class Test{

    Test._private(){
      print("In demo");
    }
    factory Test(){
      print("In demo factory");
      return Test._private();
    }
}
void main(){

  Test obj = new Test();

}

output : 
    In demo factory
    In demo

Explaination : 
  In above example , the named constructor and Factory constructor is used.
  when we create a instance of class Test then it calls the constructor named as Test() but its retrun the 
  another object so first print (In demo factory) and return the named object called to Named constructor after that print(In demo)
  
================================================================