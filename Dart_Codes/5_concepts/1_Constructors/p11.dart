
//Program 11 : 

class Test{

    Test._private(){
      print("In demo");
    }
    factory Test(){
      print("In demo factory");
      return Test._private();
    }
}
void main(){

  Test obj = new Test();

}

/* 
output : 
    In demo factory
    In demo

Explaination : 
  In above example , the named constructor and Factory constructor is used.
  when we create a instance of class Test then it calls the constructor named as Test() but its retrun the 
  another object so first print (In demo factory) and return the named object called to Named constructor after that print(In demo)
  

*/