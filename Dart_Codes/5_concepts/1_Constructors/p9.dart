
//Problem : 9


int a = 10;
class Test{
  int x = 20;
  int y = 20;
  static num z = 30;

  Test(this.x, this.y,this.a);

  void fun(){
    print(a);
    print(x);
    print(y);
  }
}

void main(){
  Test obj = new Test(10,30,40);
  obj.fun();
}

/*
Output :
      Error: 'a' isn't an instance field of this class.
      Test(this.x, this.y,this.a);

Explaination : 
      In above code , a declared as global variable and x,y,z are intance variables of Test class 
      so we access the a in constructor which is a global and it is instance foeld of class so 
      we use only variables that are in class


*/