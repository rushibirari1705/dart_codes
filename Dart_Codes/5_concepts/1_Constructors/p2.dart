
// problem 2:

class Employee{
  int? empId;
  String? empName;

  Employee(){}

  Employee(int? empId, String? empName){}

}

void main(){
  Employee obj = new Employee();
}

/*
Error : 'Employee' is already declared in this scope.
      Employee(int? empId, String? empName){}
      ^^^^^^^^
    p2.dart:8:3: Context: Previous declaration of 'Employee'.
      Employee(){}
      ^^^^^^^^
    p2.dart:15:22: Error: Can't use 'Employee' because it is declared more than once.
      Employee obj = new Employee();

    In this program we cannot use same named constructor in dart & also Employee constructor is declared more than once 
    that's why compiler gives error.

    solution : for this problem is used Named Constructor 

*/