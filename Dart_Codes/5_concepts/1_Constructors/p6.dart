
// problem 6 :
class Company{
  int? empCount;
  String? compName;

  Company({this.empCount,this.compName = "Core2web"});

  void compInfo(){
    print(empCount);
    print(compName);
  }
}

void main(){
  Company obj1 = new Company(empCount : 100, compName: "Veritas");

  Company obj2 = new Company(compName: "Pubmatic",empCount: 200);

  obj1.compInfo();
  obj2.compInfo();
}

/*
  Output : 
    100
    Veritas 
    200     
    Pubmatic

  Explaination : 
      In this program there is concept of Named Argument wher we can change the sequence of arguments
      while passing to constructor so it directly match the varible name and store value to intance variable
*/