
//Program 10 : 

class Demo{

    Demo(){
      print("In demo");
    }

    factory Demo(){
      print("In demo factory");
      return Demo();
    }
}
void main(){

  Demo obj = new Demo();

}

/*
Output: 
    Error: 'Demo' is already declared in this scope.
    factory Demo(){
            ^^^^
      Context: Previous declaration of 'Demo'.
    Demo(){
    ^^^^
    Error: Can't use 'Demo' because it is declared more than once.
      return Demo();
             ^
    Error: Can't use 'Demo' because it is declared more than once.
  Demo obj = new Demo();

Explaination:

  In this example , The Demo costructor in declared more than once so it gives error
*/