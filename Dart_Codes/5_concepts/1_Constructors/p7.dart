
//program 7 :

class Point {
  int x;
  int y;
}

void main() {
  Point obj = new Point();
}

/*

Output : 
  Error: Field 'x' should be initialized because its type 'int' doesn't allow null.
  Error: Field 'y' should be initialized because its type 'int' doesn't allow null.

Explaination : 

Both variables are not initialized and int does not allow null values.
so need to give value to both variables

*/