
// Program 4 :

class Company{
  int empCount;
  String compName;

  Company(this.empCount, [this.compName = "Biencaps"]);

  void compInfo(){
    print(empCount);
    print(compName);
  }
}

void main(){
  Company obj1 = new Company(100);

  Company obj2 = new Company(200,"Pubmatic");

  obj1.compInfo();
  obj2.compInfo();
}

/*
Output :  100
          Biencaps
          200     
          Pubmatic

Explaination : 
        In this problem Class Company has Constructor with two parameters among them one is default value
        if the user doesn't give a value to constructor them the default value is considered and store value 
        particular variable
        compName is given with default value so in Obj1 = we pass only empCount so they second value is considered
        default value:

*/