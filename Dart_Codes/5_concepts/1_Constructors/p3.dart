
// program 3: 

class Demo{
  final int? x;
  final String? str;

  const Demo(this.x, this.str);

}
void main(){

  Demo obj1 = const Demo(10,"Core2web");

  print(obj1.hashCode);

  Demo obj2 = const Demo(10,"Biencaps");

  print(obj2.hashCode);
}

/*

output : 889665570
         651191664
    
Explaination:
    This program gives hashCode for both objects and both are different from each other
    we use const constructor and see the hashCode of multiple objects  
*/