// Program 2 :

class Demo {
  void fun();
}

class Demo1 implements Demo {
  void fun() {}

}

void main() {
  Demo obj = Demo1();
}

/*
Output  : 
  Error : 
      Class Demo needs to be abstract or give implementation of fun().
*/
