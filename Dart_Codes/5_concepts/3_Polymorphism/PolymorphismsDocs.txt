
Polymorphism 

================================================================
// Program 1 :

class Company {
  void companyName() {
    print('Google');
  }
}

class Employee extends Company {
  void companyName() {
    print('Apple');
  }
}

void main() {
  Company obj = Employee();
  obj.companyName();
}

/*
  Output  : 
      Apple
  
 */
================================================================
// Program 2 :

class Demo {
  void fun();
}

class Demo1 implements Demo {
  void fun() {}

}

void main() {
  Demo obj = Demo1();
}

/*
Output  : 
  Error : 
      Class Demo needs to be abstract or give implementation of fun().
*/

================================================================
// Program 3 :
class Test {
  double x = 10.7;

  num fun() {
    return x;
  }
}

class Child extends Test {
  int y = 9;

  int fun() {
    var x = super.fun();
    print(x.runtimeType);
    x = 10;
    return 10;
  }
}

void main() {
  Child obj = new Child();
  obj.fun();
}

/*
Output : 
    double
*/

================================================================
// Program 4 :

class Test {
  int? x;

  Test(this.x);

  void fun() {
    this.x = 99;
  }
}

class Test2 extends Test {
  int? x;
  Test2(this.x, int y) : super(y);

  void fun() {
    print(x);
    super.fun();
    print(x);
    print(super.x);
  }
}

void main() {
  Test2 obj = Test2(4, 6);
  obj.fun();
}

/*
  Output: 
      4
      99
      6
*/
================================================================
// Program 5 :

class Test {
  void fun() {
    print("Test class");
  }

  static void gun() {
    print("In static test");
  }
}

class Test2 extends Test {
  @override
  int fun() {
    super.fun();
    return 10;
  }

  @override
  void gun() {
    print("In test2 gun");
    super.gun();
  }
}

void main() {
  Test2 obj = Test2();
  obj.fun();
}

/*
  Output : 
  Error: Superclass has no method named 'gun'.
    super.gun();
*/
================================================================
// Program 6 :

class Parent {
  int x = 10;

  Parent();
  void getData() {
    print(x);
  }

}

class Child extends Parent {
  double y = 10;
  static String str = "core2web";

  Child();

  @override
  int getData() {
    print(y);
    return 10;
  }

}

void main() {
  Child obj = new Child();
  obj.getData();
}

/*
  Output :  10.0
*/
================================================================
// Program 7  :

abstract class Parent {
  int x;
  int y = 7;

  Parent(this.x);

  void printData() {
    print(x);
  }
}

class Child extends Parent {

  Child(int x, int y) : super(y);

  int printData() {
    print(x);
    print(y);
    return 3;
  }

}

void main() {
  Child obj = new Child(6, 7);
  obj.printData();
}

/*
  Output : 
      7
      7
*/
================================================================
//Program 8 :

class Test {
  int x = 6;
  Test();

  void printData() {
    print(x);
  }
}

class Test2 {
  int x = 9;
  Test2() {
    print("In test2");
  }
}

class Child extends Test implements Test2 {
  int x = 7;
  void printData() {
    print(super.x);
    super.printData();
  }
}

void main() {
  Child obj = new Child();
  obj.printData();
}

/*
  Output  : 
        6
        7
*/
================================================================
// Program 9 :

abstract class Test {
  void build();
}

class Test2 extends Test {
  @override
  void build() {
    super.build();
  }
}

void main(){
  Test2 obj =  new Test2();
  obj.build();
}

/*
  Output : 
      Error: Superclass has no method named 'build'.
      super.build();

*/
================================================================

// Program 10:

abstract class Test {
  int x;
  Test(this.x) {
    print("In constructor");
  }
  void fun() {
    print("Fun");
  }

  void gun();
}

class Test2 extends Test {
  Test2(super.x);
  void gun() {
    print("In gun");
  }
}

void main() {
  Test2 obj = new Test2(10);
  obj.fun();
  obj.gun();
}

/*

Output : 
  In constructor
  Fun    
  Inn gun
*/
