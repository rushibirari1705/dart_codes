// Program 9 :

abstract class Test {
  void build();
}

class Test2 extends Test {
  @override
  void build() {
    super.build();
  }
}

void main(){
  Test2 obj =  new Test2();
  obj.build();
}

/*
  Output : 
      Error: Superclass has no method named 'build'.
      super.build();

*/