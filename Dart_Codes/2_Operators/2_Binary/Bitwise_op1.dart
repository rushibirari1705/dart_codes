
// Bitwise operators in dart  : & , | , ^ , ~ , << , >>
/*
    Note :   & , | , ^ , ~ , << , >>
*/

main(){
  // All bitwise operators

  int x = 5 ;
  int y = 3;

  print(x & y); // & 

  print(x | y) ; // |

  print(x ^ y); // ^

  print(x << 2); // left shift operator

  print(y >> 3); // right shift operator


}