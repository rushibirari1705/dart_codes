
// Relational operators in dart : < , > , <= , >= , == , != 
/*
    Note : < , > , <= , >= , == , != 
 */

main(){
  // All relational operators 

  int x = 15;
  int y = 20;

  print(x < y);
  print(x > y);
  print(x <= y);
  print(x >= y);
  print(x == y);
  print(x != y);


}