
// Logical operators in dart

/*  Note :
        logical AND : &&
        logical OR : ||
        logical NOT : !

*/

main(){
  // Logical operators

  int x = 30;
  int y = 20;
/*
  print( x  && y); //Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
  print( x  || y); //Error: A value of type 'int' can't be assigned to a variable of type 'bool'.
*/

// Note : Logical operators need True or False as operands

print((x > y ) && (x < y)); // false

print((x > y ) && (x >= y)); // true

print((x > y ) || (x < y)); // true

print((x > y ) || (x >= y)); // true


}