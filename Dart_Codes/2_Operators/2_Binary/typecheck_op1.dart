
// Typecheck operators in Dart :  as , is ,  is! 

main(){
  // typecheck operators

  int x = 10;
  double y = 20.5;
  num z = 30.8;

  print(x is double); // false

  print(y is int ); // false

  print(y is! int);// true

  print(x is! double);// true

  print(z is double); //true
  
}