
//All Arithmetic operators in dart : +  , -  , *  ,  / ,  % ,~/
/*   Note :
            +  , -  , *  ,  / ,  % ,~/
*/

main(){

  int x = 110 ,y = 201;
  // Addition
  int sum = x + y;
  print("Addition is : $sum ");

  // subtraction
  int sub = x - y; 
  print("Subtraction is : $sub ");

  //Multiplication
  int mul = x * y;
  print("Multiplication is : $mul");

  //Division
  num div = x / y;
  print("Division is : $div");

  // Modulus
  int mod = x % y;
  print("Modulus is : $mod");

  //Integer division
  int div1 = x  ~/ y ;
  print("Integer division is : $div1");

}