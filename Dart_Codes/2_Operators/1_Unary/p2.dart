
/* Unary Operator decrement
    1: Pre Decrement (--x)
    2: Post Decrement (x--)
*/

main(){
  // Pre & post decrement

  int x = 10;

  print(--x); // 9

  print(x--);// 9

  print(x); // 8

  int ans, y = 5;

  

  ans = y-- + y--;
  print(ans); // 9

  ans = y-- - y--;
  print(ans); // 1



}