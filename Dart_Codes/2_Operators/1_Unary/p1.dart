
// Unary operator 
/*Note :  1: pre Increment (++x)
          2: Post Increment (x++)
          3: Pre Decrement (--x)
          4: Post Decrement (x--)

  */

  main(){
    //  increment

    int x = 10;
    print(++x); // 11

    print(x++); // 11

    print(x); // 12


    int ans;
    int y = 5;

    ans = y++ + y++;
    print(ans);// 11

    ans = ++y + ++y ;
    print(ans); //27


  }